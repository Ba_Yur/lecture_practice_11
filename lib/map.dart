import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
List<CameraDescription> cameras;


class Map extends StatefulWidget {
  @override
  State<Map> createState() => MapState();
}

class MapState extends State<Map> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  double latitude;
  double longitude;

  Future<LocationData> locData = Location().getLocation();

  Future<LocationData> getCurrentUserLocation() async {
    final locData = await Location().getLocation();
    setState(() {
      latitude = locData.latitude;
      longitude = locData.longitude;
    });

    print(locData.latitude);
    print(locData.longitude);
  }

  @override
  void initState() {
    getCurrentUserLocation();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Stack(
        children: [
          Container(
            child: GoogleMap(
              mapType: MapType.hybrid,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
            height: 500,
          ),
          Positioned(
            bottom: 0,
            child: ElevatedButton(
              onPressed: getCurrentUserLocation,
              child: Text('open map'),
            ),
          ),
        ],
      ),
    );
  }


}